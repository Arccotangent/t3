#include <iostream>
#include <random>
#include <sstream>
#include <unistd.h>
#include "t3.hpp"
using namespace std;

char t3::board[9] =
{
	' ', ' ', ' ',
	' ', ' ', ' ',
	' ', ' ', ' '
};

bool t3::turn = false; //false = X, true = O
int t3::peco = 0;
int t3::pecx = 0;
bool t3::game_active = true;
int t3::exitfac = 0;

void t3::clear_board()
{
	system("clear");
}

bool t3::getTurn()
{
	return turn;
}

void t3::reset_board()
{
	board[0] = ' ';
	board[1] = ' ';
	board[2] = ' ';
	board[3] = ' ';
	board[4] = ' ';
	board[5] = ' ';
	board[6] = ' ';
	board[7] = ' ';
	board[8] = ' ';
}

bool t3::is_occupied(int pos)
{
	char tty4 = board[pos];
	bool tty5;
	if (tty4 == 32)
		tty5 = false;
	else
		tty5 = true;
	return tty5;
}

bool t3::board_full()
{
	if (board[0] != ' ' && board[1] != ' ' && board[2] != ' ' && board[3] != ' ' && board[4] != ' ' && board[5] != ' ' && board[6] != ' ' && board[7] != ' ' && board[8] != ' ')
		return true;
	else
		return false;
}

bool t3::poboard(int num)
{
	int tty1 = 0;
	for (int i = 0; i < 9; i++)
	{
		bool tty6 = is_occupied(i);
		if (tty6 == true)
			tty1++;
	}
	if (tty1 >= num)
		return true;
	else
		return false;
}

int t3::print_board()
{
	clear_board();
	stringstream t3ss;
	t3ss << board[0] << " | " << board[1] << " | " << board[2] << endl;
	t3ss << "--+---+--" << endl;
	t3ss << board[3] << " | " << board[4] << " | " << board[5] << endl;
	t3ss << "--+---+--" << endl;
	t3ss << board[6] << " | " << board[7] << " | " << board[8] << endl;
	cout << t3ss.str();
	if (!board_full())
		cout << "\a";
	return 0;
}

int t3::gnpos()
{
	int tty3;
	if (poboard(6))
	{
		if (!is_occupied(0))
			tty3 = 0;
		else if (!is_occupied(1))
			tty3 = 1;
		else if (!is_occupied(2))
			tty3 = 2;
		else if (!is_occupied(3))
			tty3 = 3;
		else if (!is_occupied(4))
			tty3 = 4;
		else if (!is_occupied(5))
			tty3 = 5;
		else if (!is_occupied(6))
			tty3 = 6;
		else if (!is_occupied(7))
			tty3 = 7;
		else if (!is_occupied(8))
			tty3 = 8;
	}
	else
	{
		random_device urand;
		int tty2 = urand() % 4;
		if (tty2 == 0)
		{
			if (board[4] == ' ')
				tty3 = 4;
			else if (board[8] == ' ')
				tty3 = 8;
			else if (board[0] == ' ')
				tty3 = 0;
			else if (board[6] == ' ')
				tty3 = 6;
		}
		else if (tty2 == 1)
		{
			if (board[4] == ' ')
				tty3 = 4;
			else if (board[0] == ' ')
				tty3 = 0;
			else if (board[8] == ' ')
				tty3 = 8;
			else if (board[6] == ' ')
				tty3 = 6;
		}
		else if (tty2 == 2)
		{
			if (board[4] == ' ')
				tty3 = 4;
			else if (board[2] == ' ')
				tty3 = 2;
			else if (board[0] == ' ')
				tty3 = 0;
			else if (board[8] == ' ')
				tty3 = 8;
		}
		else if (tty2 == 3)
		{
			if (board[4] == ' ')
				tty3 = 4;
			else if (board[0] == ' ')
				tty3 = 0;
			else if (board[2] == ' ')
				tty3 = 2;
			else if (board[8] == ' ')
				tty3 = 8;
		}
	}
	return tty3;
}

int t3::play_x(int pos)
{
	if (board[pos] == ' ' && !turn)
	{
		board[pos] = 'X';
		turn = true;
	}
	else
		return 1;
	return 0;
}

int t3::play_o(int pos)
{
	if (board[pos] == ' ' && turn)
	{
		board[pos] = 'O';
		turn = false;
	}
	else
		return 1;
	return 0;
}

void t3::auto_ai()
{
	random_device r;
	if (turn) //true = o, false = x
	{
		if ((((board[1] == 'X' && board[2] == 'X') || (board[3] == 'X' && board[6] == 'X') || (board[4] == 'X' && board[8] == 'X')) && board[0] != 'O') == true)
			peco = play_o(0);
		else if ((((board[0] == 'X' && board[2] == 'X') || (board[4] == 'X' && board[7] == 'X')) && board[1] != 'O') == true)
			peco = play_o(1);
		else if ((((board[0] == 'X' && board[1] == 'X') || (board[5] == 'X' && board[8] == 'X') || (board[4] == 'X' && board[6] == 'X')) && board[2] != 'O') == true)
			peco = play_o(2);
		else if ((((board[0] == 'X' && board[6] == 'X') || (board[4] == 'X' && board[5] == 'X')) && board[3] != 'O') == true)
			peco = play_o(3);
		else if ((((board[0] == 'X' && board[8] == 'X') || (board[3] == 'X' && board[5] == 'X') || (board[6] == 'X' && board[2] == 'X') || (board[1] == 'X' && board[7] == 'X')) && board[4] != 'O') == true)
			peco = play_o(4);
		else if ((((board[2] == 'X' && board[8] == 'X') || (board[3] == 'X' && board[4] == 'X')) && board[5] != 'O') == true)
			peco = play_o(5);
		else if ((((board[0] == 'X' && board[3] == 'X') || (board[2] == 'X' && board[4] == 'X') || (board[7] == 'X' && board[8] == 'X')) && board[6] != 'O') == true)
			peco = play_o(6);
		else if ((((board[6] == 'X' && board[8] == 'X') || (board[1] == 'X' && board[4] == 'X')) && board[7] != 'O') == true)
			peco = play_o(7);
		else if ((((board[2] == 'X' && board[5] == 'X') || (board[0] == 'X' && board[4] == 'X') || (board[0] == 'X' && board[4] == 'X')) && board[8] != 'O') == true)
			peco = play_o(8);
		else
			peco = play_o(gnpos());
	}
	else
	{
		if ((((board[1] == 'O' && board[2] == 'O') || (board[3] == 'O' && board[6] == 'O') || (board[4] == 'O' && board[8] == 'O')) && board[0] != 'X') == true)
			pecx = play_x(0);
		else if ((((board[0] == 'O' && board[2] == 'O') || (board[4] == 'O' && board[7] == 'O')) && board[1] != 'X') == true)
			pecx = play_x(1);
		else if ((((board[0] == 'O' && board[1] == 'O') || (board[5] == 'O' && board[8] == 'O') || (board[4] == 'O' && board[6] == 'O')) && board[2] != 'X') == true)
			pecx = play_x(2);
		else if ((((board[0] == 'O' && board[6] == 'O') || (board[4] == 'O' && board[5] == 'O')) && board[3] != 'X') == true)
			pecx = play_x(3);
		else if ((((board[0] == 'O' && board[8] == 'O') || (board[3] == 'O' && board[5] == 'O') || (board[6] == 'O' && board[2] == 'O') || (board[1] == 'O' && board[7] == 'O')) && board[4] != 'X') == true)
			pecx = play_x(4);
		else if ((((board[2] == 'O' && board[8] == 'O') || (board[3] == 'O' && board[4] == 'O')) && board[5] != 'X') == true)
			pecx = play_x(5);
		else if ((((board[0] == 'O' && board[3] == 'O') || (board[2] == 'O' && board[4] == 'O') || (board[7] == 'O' && board[8] == 'O')) && board[6] != 'X') == true)
			pecx = play_x(6);
		else if ((((board[6] == 'O' && board[8] == 'O') || (board[1] == 'O' && board[4] == 'O')) && board[7] != 'X') == true)
			pecx = play_x(7);
		else if ((((board[2] == 'O' && board[5] == 'O') || (board[0] == 'O' && board[4] == 'O') || (board[0] == 'O' && board[4] == 'O')) && board[8] != 'X') == true)
			pecx = play_x(8);
		else
			pecx = play_x(gnpos());
	}
	if (peco == 1 || pecx == 1)
		check_win();
	print_board();
	usleep(50000);
}

void t3::check_win()
{
	if (exitfac >= 10)
	{
		if ((board[0] == 'X' && board[1] == 'X' && board[2] == 'X') || (board[0] == 'X' && board[4] == 'X' && board[8] == 'X') || (board[0] == 'X' && board[3] == 'X' && board[6] == 'X') || (board[1] == 'X' && board[4] == 'X' && board[7] == 'X') || (board[2] == 'X' && board[4] == 'X' && board[6] == 'X') || (board[2] == 'X' && board[5] == 'X' && board[8] == 'X') || (board[3] == 'X' && board[4] == 'X' && board[5] == 'X') || (board[6] == 'X' && board[7] == 'X' && board[8] == 'X'))
		{
			print_board();
			cout << "\nX wins!" << endl;
			usleep(1000000);
			reset_board();
			peco = 0;
			pecx = 0;
			exitfac = 0;
			//exit(0);
		}
		else if ((board[0] == 'O' && board[1] == 'O' && board[2] == 'O') || (board[0] == 'O' && board[4] == 'O' && board[8] == 'O') || (board[0] == 'O' && board[3] == 'O' && board[6] == 'O') || (board[1] == 'O' && board[4] == 'O' && board[7] == 'O') || (board[2] == 'O' && board[4] == 'O' && board[6] == 'O') || (board[2] == 'O' && board[5] == 'O' && board[8] == 'O') || (board[3] == 'O' && board[4] == 'O' && board[5] == 'O') || (board[6] == 'O' && board[7] == 'O' && board[8] == 'O'))
		{
			print_board();
			cout << "\nO wins!" << endl;
			usleep(1000000);
			reset_board();
			peco = 0;
			pecx = 0;
			exitfac = 0;
			//exit(0);
		}
		else if (board_full())
		{
			print_board();
			cout << "\nStalemate." << endl;
			usleep(1000000);
			reset_board();
			peco = 0;
			pecx = 0;
			exitfac = 0;
			//exit(0);
		}
	}
	else
		exitfac++;
}
