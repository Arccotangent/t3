#ifndef T3_HPP
#define T3_HPP
#include <iostream>
using namespace std;

class t3
{
	public:
		void clear_board();
		int print_board();
		int play_x(int pos);
		int play_o(int pos);
		void auto_ai();
		void check_win();
		void reset_board();
		bool board_full();
		int gnpos();
		bool poboard(int num);
		bool is_occupied(int pos);
		static bool game_active;
		bool getTurn();
	private:
		static char board[9];
		static bool turn; //false = x, true = o
		static int peco, pecx, exitfac;
};

#endif // T3_HPP
