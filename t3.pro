TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    t3.cpp

HEADERS += \
    t3.hpp

QMAKE_CXXFLAGS += -std=c++14
