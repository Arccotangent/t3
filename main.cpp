#include <iostream>
#include <unistd.h>
#include <random>
#include "t3.hpp"
using namespace std;

int main()
{
	cout << "T3 v3.0" << endl;
	cout << endl;
	t3 main;
	malloc(65536);
	cout << "Number of players: ";
	int np;
	cin >> np;
	if (np == 0)
	{
		while (true)
		{
			main.auto_ai();
		}
	}
	else if (np == 1)
	{
		cout << "Do you want to go first? (y/n): ";
		char yn;
		cin >> yn;
		if (yn == 'Y' || yn == 'y')
		{
			cout << "You are X" << endl;
			usleep(1000000);
			while (main.game_active)
			{
				main.check_win();
				if (main.getTurn())
				{
					main.clear_board();
					main.print_board();
					main.check_win();
					main.auto_ai();
				}
				else
				{
					main.clear_board();
					main.print_board();
					main.check_win();
					if (!main.board_full())
					{
						cout << "Position? (1-9): ";
						int p;
						cin >> p;
						if (p >= 1 && p <= 9)
							main.play_x(p - 1);
						else
							cout << "Bad position! 1-9 only!" << endl;
					}
					else
						main.check_win();
				}
			}
		}
		else
		{
			cout << "You are O" << endl;
			usleep(1000000);
			main.clear_board();
			main.print_board();
			while (main.game_active)
			{
				main.clear_board();
				main.print_board();
				main.check_win();
				if (!main.getTurn())
				{
					main.clear_board();
					main.print_board();
					main.check_win();
					main.auto_ai();
				}
				else
				{
					main.clear_board();
					main.print_board();
					main.check_win();
					if (!main.board_full())
					{
						cout << "Position? (1-9): ";
						int p;
						cin >> p;
						if (p >= 1 && p <= 9)
							main.play_o(p - 1);
						else
							cout << "Bad position! 1-9 only!" << endl;
					}
					else
						main.check_win();
				}
				main.clear_board();
				main.print_board();
			}
		}
	}
	else if (np == 2)
	{
		cout << "X will go first." << endl;
		usleep(1000000);
		main.clear_board();
		main.print_board();
		main.check_win();
		while (main.game_active)
		{
			main.clear_board();
			main.print_board();
			main.check_win();
			if (!main.board_full())
			{
				if (!main.getTurn())
					cout << "It is X's turn." << endl;
				else
					cout << "It is O's turn." << endl;
				cout << "Position? (1-9): ";
				int p;
				cin >> p;
				if (p >= 1 && p <= 9)
				{
					if (!main.getTurn())
						main.play_x(p - 1);
					else
						main.play_o(p - 1);
				}
				else
					cout << "Bad position! 1-9 only!" << endl;
				main.clear_board();
				main.print_board();
				main.check_win();
			}
			else
				main.check_win();
		}
		main.clear_board();
		main.print_board();
		main.check_win();
	}
	else
	{
		cerr << "Invalid player count! Exiting." << endl;
		return 1;
	}
	return 0;
}

